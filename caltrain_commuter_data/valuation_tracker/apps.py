from django.apps import AppConfig


class ValuationTrackerConfig(AppConfig):
    name = 'valuation_tracker'
