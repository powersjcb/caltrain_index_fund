from django.contrib.gis.db import models
from model_utils.models import TimeStampedModel

class CompanyOffice(TimeStampedModel, models.Model):
    name = models.CharField(max_length=255)
    geo_coord= models.PointField(null=True)

    class Meta:
        db_table = 'office'


