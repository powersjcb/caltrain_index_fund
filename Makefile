PIP_INSTALL=pip install

all:
ifndef VIRTUAL_ENV
	@echo "You need make and activate a virtualenv first:"
	@echo "make venv"
	@echo "source venv/bin/activate"
	@exit
else
	@$(MAKE) build
endif

pip:
	@$(PIP_INSTALL) --upgrade pip
	@$(PIP_INSTALL) -r requirements.txt

venv:
	@virtualenv --python=python3.5 venv
