#!/bin/bash

# base dependencies
brew install python python3 node wget postgresql
createdb $USER
sudo pip install virtualenv

# geospatial dependencies
brew install postgis


